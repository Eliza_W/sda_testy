#include  "gtest/gtest.h"
#include "functions.h"

TEST(TestingSlownie, Zero){
	EXPECT_STREQ("zero", slownieLiczby(0).c_str());// zmienia na string typu C, czyli tablice znakow
}
TEST(TestingSlownie, One){
	EXPECT_STREQ("jeden jeden ", slownieLiczby(11).c_str());
}
TEST(TestingSlownie, LongLastZero){
	EXPECT_STREQ("jeden dwa trzy cztery piec szesc siedem osiem zero ", slownieLiczby(123456780).c_str());// zle jesli zero jest ostatnie

}
TEST(TestingSlownie, LongLastZeroZeroZero){
	EXPECT_STREQ("jeden dwa trzy cztery piec szesc zero zero zero ", slownieLiczby(123456000).c_str());// zle jesli zero jest ostatnie

}
TEST(TestingSlownie, Negative){
	EXPECT_STREQ("minus jeden cztery zero osiem ", slownieLiczby(-1408).c_str());
}
TEST(TestingSlownie, Long){
	EXPECT_STREQ("jeden dwa trzy cztery piec szesc siedem zero osiem ", slownieLiczby(123456708).c_str());// zle jesli zero jest ostatnie

}

